// EJS doesnt really need webpack, :) 
const path = require('path');


module.exports = {
    mode: 'development',
    entry: {
        home: './src/js/index_scripts.js',
        iot_list: './src/js/iot_list_scripts.js',
    },
    output: {
        filename: '[name].js',
        sourceMapFilename: "[name].js.map",
        libraryTarget: "umd", // universal module definition
        path: path.resolve(__dirname, 'dist')
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    }

}
