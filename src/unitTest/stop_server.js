if (typeof callback === "function") {
    // Call it, since we have confirmed it is callable​
    let data= '{"StoppingInstances": [{"InstanceId": "i-1234567890abcdef0","CurrentState": {"Code": 64,"Name": "stopping"},"PreviousState": {"Code": 16, "Name": "running"}}]}';

    callback(null, data);
}