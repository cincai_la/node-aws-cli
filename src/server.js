// Get the package we need
const path = require('path');
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const pino = require('pino');
const expressPino = require('express-pino-logger');

const logger = pino({ level: process.env.LOG_LEVEL || 'info', prettyPrint: true});
const expressLogger = expressPino({ logger });
const ec2controller = require('./controller/ec2_controller');
const iotController = require('./controller/iot_core_controller');
const rdsController = require('./controller/rds_controller');
const SqlLiteConfig = require('./dataAccessObject/config/sqlite_config');
const indexTableCalls = require('./views/js_model/index_table_obj');
const s3Controller = require('./controller/s3_controller');
const errHandling = require('./stf_generic_js/err_handling');
const IndObjs = require('./views/js_model/indexObjs');

const mySqlLiteConfig = new SqlLiteConfig();
const { sessionSetting } = mySqlLiteConfig;

// Create our Express application, with logger
const app = express();
app.use(session(sessionSetting));
app.use(expressLogger);

// set the view engine to ejs
app.engine('html', require('ejs').renderFile);

app.set('view engine', 'html');

// Use the body-parser package in our application
app.use(bodyParser.urlencoded({
  extended: true,
}));

// User environment defined port or 8081
const port = process.env.PORT || 5001;

// Create our Express router
const router = express.Router();

// Initial dashboard route
router.get('/', (req, res) => {
  const indexObjs = new IndObjs.IndexObjs(req);
  indexObjs.setReqSession(req);
  res.render(path.join(__dirname, '/views/index.html'), {
    index_table: indexObjs.index_ec2_table_obj_list,
    index_rds_table: indexObjs.index_rds_table_obj_list,
  });
});

// RDS route
router.get('/rds', (req, res) => {
  const indexObjs = new IndObjs.IndexObjs(req);
  indexObjs.setReqSession(req);
  res.render(path.join(__dirname, '/views/rds_list.html'), {
    index_rds_table: indexObjs.index_rds_table_obj_list,
  });
});

router.get('/iot_core', (req, res) => {
  const indexObjs = new IndObjs.IndexObjs(req);
  // const indexObjs = new indexTableCalls.IndexObjs(req);
  if (!errHandling.isUndefinedOrNull(indexObjs.iot_table_table_obj_list)) {
    for (let i = 0; i < indexObjs.iot_table_table_obj_list.length; i += 1) {
      indexObjs.iot_table_table_obj_list[i].state = indexTableCalls
        .general_mapping.iot_state.disconnected;
    }
  }
  indexObjs.setReqSession(req);
  res.render(path.join(__dirname, '/views/iot_list.html'), {
    iot_table: indexObjs.iot_table_table_obj_list,
    inputJSONText: errHandling.checkJSONStringify(indexObjs.inputIOTJSONText),
    iot_status_strings: errHandling.checkJSONStringify(req.session.iot_status_strings),
  });
});

// Initial s3 display
router.get('/s3', (req, res) => {
  // const indexObjs = new indexTableCalls.IndexObjs(req);
  const indexObjs = new IndObjs.IndexObjs(req);
  indexObjs.setReqSession(req);
  res.render(path.join(__dirname, '/views/s3.html'), { s3_buckets: indexObjs.s3_table_obj_list });
});

// Create endpoint handlers for handling dashboard action
router.route('/dashboard_action')
  .post(ec2controller.indexPageCommand);

router.route('/iot_list_action')
  .post(iotController.iot_core_PageCommand);

router.route('/s3_action')
  .post(s3Controller.s3PageCommand);

router.route('/rds_action')
  .post(rdsController.rdsPageCommand);

app.use('/smeTechBackEnd/dist', express.static(path.join(`${__dirname}/../dist`)));
// app.use('/js_model', express.static(path.join(__dirname+'/views/js_model')));
app.use('/', router);
// Start application
app.listen(port);

logger.info(path.join(`${__dirname}/../dist`));
logger.info('Server running on port %d', port);
