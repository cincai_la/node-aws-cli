const errHandling = require('../../stf_generic_js/err_handling');

class IndexObjs {
  constructor(req) {
    this.index_ec2_table_obj_list = null;
    this.index_rds_table_obj_list = null;
    this.iot_table_table_obj_list = null;
    this.s3_table_obj_list = null;
    this.inputIOTJSONText = null;
    this.iot_status_strings = null;
    if (!errHandling.isUndefinedOrNull(req.session.index_rds_table)) {
      this.index_rds_table_obj_list = req.session.index_rds_table;
    }
    if (!errHandling.isUndefinedOrNull(req.session.index_table)) {
      this.index_ec2_table_obj_list = req.session.index_table;
    }
    if (!errHandling.isUndefinedOrNull(req.session.iot_core_table)) {
      this.iot_table_table_obj_list = req.session.iot_core_table;
    }
    if (!errHandling.isUndefinedOrNull(req.session.inputJSONText)) {
      this.inputIOTJSONText = req.session.inputJSONText;
    }
    if (!errHandling.isUndefinedOrNull(req.session.iot_status_strings)) {
      this.iot_status_strings = req.session.iot_status_strings;
    }
    if (!errHandling.isUndefinedOrNull(req.session.s3_bucket_list)) {
      this.s3_table_obj_list = req.session.s3_bucket_list;
    }
  }

  setReqSession(req) {
    // if (req.session.index_table === undefined &&
    //     err_handling.isUndefinedOrNull(this.index_ec2_table_obj_list)){
    //     this.index_ec2_table_obj_list = [new index_ec2_table_obj()];
    // }
    // if (req.session.index_rds_table === undefined &&
    //     err_handling.isUndefinedOrNull(this.index_rds_table_obj_list)){
    //     this.index_rds_table_obj_list = [new index_rds_table_obj()];
    // }
    // if (req.session.iot_core_table === undefined &&
    // err_handling.isUndefinedOrNull(this.iot_table_table_obj_list)){
    //     this.iot_table_table_obj_list = [new iot_core_table_obj()];
    // }
    req.session.inputJSONText = this.inputIOTJSONText;
    req.session.index_table = this.index_ec2_table_obj_list;
    req.session.index_rds_table = this.index_rds_table_obj_list;
    req.session.iot_core_table = this.iot_table_table_obj_list;
    req.session.iot_status_strings = this.iot_status_strings;
    req.session.s3_bucket_list = this.s3_table_obj_list;
  }
}

module.exports = {
  IndexObjs,
};
