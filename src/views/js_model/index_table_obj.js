const err_handling = require('../../stf_generic_js/err_handling');

class index_stopping_obj {
  constructor(InstanceId, CurrentState, PreviousState) {
    this.InstanceId = InstanceId;
    this.CurrentState = CurrentState;
    this.PreviousState = PreviousState;
  }
}

class index_ec2_table_obj {
  constructor(name, state_code, state_name, instance_type, InstanceId, inputPublicIPAddress) {
    if (name === undefined || InstanceId === undefined) {
      this.name = null;
      this.state_name = null;
      this.instance_type = null;
      this.state_code = null;
      this.InstanceId = null;
      this.publicIPAddress = null
    } else {
      this.name = name;
      this.state_name = state_name;
      this.instance_type = instance_type;
      this.state_code = state_code;
      this.InstanceId = InstanceId;
      this.publicIPAddress = inputPublicIPAddress;
    }
  }
}

class index_rds_table_obj {
  constructor(name, instanceId, instance_type, status) {
    if (name === undefined || instanceId === undefined) {
      this.instanceId = null;
      this.name = null;
      this.instance_type = null;
      this.status = null;
    } else {
      this.instanceId = instanceId;
      this.name = name;
      this.instance_type = instance_type;
      this.status = status;
    }
  }
}

class iot_core_table_obj {
  constructor(thingName, thingArn, iot_state) {
    if (err_handling.isUndefinedOrNull(thingName) || err_handling.isUndefinedOrNull(thingArn)) {
      this.thingName = null;
      this.thingArn = null;
      this.state = null;
    } else {
      this.thingName = thingName;
      this.thingArn = thingArn;
      if (iot_state in general_mapping.iot_state) {
        this.state = iot_state;
      } else {
        this.state = general_mapping.iot_state.disconnect;
      }
    }
  }
}

class s3_table_obj {
  constructor(created_date, created_time, bucket_name) {
    if (err_handling.isUndefinedOrNull(created_date) || err_handling.isUndefinedOrNull(bucket_name)) {
      this.s3_created_date = null;
      this.s3_created_time = null;
      this.s3_bucket_name = null;
    } else {
      this.s3_created_date = created_date;
      this.s3_created_time = created_time;
      this.s3_bucket_name = bucket_name;
    }
  }
}

class general_mapping {
}

// Static variable shared by all instances
general_mapping.index_func = {
  describe_ec2_instances: 'describe_ec2_instances',
  stop_ec2_instances: 'stop_ec2_instances',
  start_ec2_instances: 'start_ec2_instances',
  describe_rds_instances: 'describe_rds_instances',
  stop_rds_instances: 'stop_rds_instances',
  start_rds_instances: 'start_rds_instances',
  list_iot_things: 'list_iot_things',
  connect_iot: 'connect_iot',
  disconnect_iot: 'disconnect_iot',
  update_iot: 'update_iot',
  get_iot: 'get_iot',
  get_json_iot: 'get_json_iot',
  get_s3_buckets: 'get_s3_buckets',
};

general_mapping.iot_state = {
  disconnected: 'disconnected',
  connected: 'connected',
};

module.exports = {
  general_mapping,
  IndexRdsTableObj : index_rds_table_obj,
  IndexEc2TableObj : index_ec2_table_obj,
  IndexStoppingObj : index_stopping_obj,
  iot_core_table_obj,
  S3TableObj : s3_table_obj,
};
