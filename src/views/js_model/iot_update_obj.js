/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

//node.js deps

//npm deps

//app deps
const thingShadow = require('aws-iot-device-sdk').thingShadow;
const path = require('path');
const stf_sqliteDAO = require('../../dataAccessObject/stf_sqliteDAO');

//begin module

//
// Simulate the interaction of a mobile device and a remote thing via the
// AWS IoT service.  The remote thing will be a dimmable color lamp, where
// the individual RGB channels can be set to an intensity between 0 and 255.
// One process will simulate each side, with testMode being used to distinguish
// between the mobile app (1) and the remote thing (2).  The remote thing
// will update its state periodically using an 'update thing shadow' operation,
// and the mobile device will listen to delta events to receive the updated
// state information.
//


function processTest (thingName){

    if (!(this instanceof processTest)) {
        return new processTest(thingName);
    }


    this.args = {
            privateKey: path.join(__dirname, '../../dataAccessObject/certs/a28ba782b0-private.pem.key'),
            clientCert: path.join(__dirname, '../../dataAccessObject/certs/a28ba782b0-certificate.pem.crt'),
            caCert: path.join(__dirname, '../../dataAccessObject/certs/AmazonRootCA1.pem.txt'),
            clientId: 'STF_UP_iotClient',
            /* milliseconds */
            baseReconnectTimeMs: 4000,
            /* seconds */
            keepAlive: 300,
            /* milliseconds */
            delay: 4000,
            protocol: 'mqtts',
            Port: 8883,
            Host: 'a216loigocy5v5-ats.iot.ap-southeast-2.amazonaws.com',
            Debug: false,
            testMode: 1
        };

    this.thingShadows = thingShadow({
        keyPath: this.args.privateKey,
        certPath: this.args.clientCert,
        caPath: this.args.caCert,
        clientId: this.args.clientId,
        region: this.args.region,
        baseReconnectTimeMs: this.args.baseReconnectTimeMs,
        keepalive: this.args.keepAlive,
        protocol: this.args.protocol,
        port: this.args.Port,
        host: this.args.Host,
        debug: this.args.Debug
    });
    //
    // Operation timeout in milliseconds
    //
    this.operationTimeout = 10000;

    this.thingName = thingName;
    this.currentTimeout = null;

    // client tokens with their respective thing shadows.
    //
    this.stack = [];

    this.thingShadows.on('connect', function() {
        console.log('connected to AWS IoT');
    });

    this.thingShadows.on('close', function() {
        console.log('close');
        this.thingShadows.unregister(thingName);
    });

    this.thingShadows.on('reconnect', function() {
        console.log('reconnect');
    });


    this.thingShadows.on('error', function(error) {
        console.log('error', error);
    });

    this.thingShadows.on('message', function(topic, payload) {
        console.log('message', topic, payload.toString());
    });

}

processTest.prototype.handleDelta = function(thingName, stateObject) {
    let state_text = JSON.stringify(stateObject);
    console.log('delta on: ' + thingName + state_text);
    stf_sqliteDAO.updateIOTTable(thingName, state_text, null, function(err){
        if (err){
            console.log(err);
        }
    });
};

/**
 * @param callback is function(err, failed topic)
 */
processTest.prototype.myDeviceConnect= function(callback) {
    this.thingShadows.register(this.thingName, {
            ignoreDeltas: false,
            persistentSubscribe: true
        },callback);
};

processTest.prototype.handleStatus = function(thingName, stat, clientToken, stateObject) {
    let expectedClientToken = this.stack.pop();

    if (expectedClientToken === clientToken) {
        let stateInText = JSON.stringify(stateObject);
        stf_sqliteDAO.updateIOTTable(thingName, null, stateInText, function(err){
            if (err){
                console.log(err);
            }
        });
        console.log('got \'' + stat + '\' status on: ' + thingName);
        console.log('message: ' +  stateInText);
    } else {
        console.log('(status) client token mismtach on: ' + thingName);
    }
};


processTest.prototype.myDeviceUnregister= function() {
     this.thingShadows.unregister(this.thingName);
     console.log("things are now unregistered");
};

/**
 * @param inputState is JSON text for AWS IOT
 * @param callback is function(err, client_token)
 */
processTest.prototype.updateThing= function(inputState, callback) {
    let clientToken = this.thingShadows['update'](this.thingName, inputState);

    if (clientToken === null) {
        //
        // The thing shadow cant handle it probably due to other operation. Please wait
        if (this.currentTimeout !== null) {
            console.log('operation in progress, scheduling retry...');
            this.currentTimeout = setTimeout(
                function() {
                    this.updateThing(inputState);
                },
                this.operationTimeout * 2);
        }
    } else {
        //
        // Save the client token so that we know when the operation completes.
        //
        callback(null, clientToken);
    }
};

/**
 * @param callback is function(err, client_token)
 */
processTest.prototype.get_IOT_status = function (callback){
    let clientToken = this.thingShadows['get'](this.thingName);
    callback(null, clientToken);
};

/**
 * @param thingName
 * @param clientToken
 */
processTest.prototype.handleTimeout = function (thingName, clientToken) {
    let expectedClientToken = this.stack.pop();

    if (expectedClientToken === clientToken) {
        console.log('timeout on: ' + thingName);
    } else {
        console.log('(timeout) client token mismtach on: ' + thingName);
    }
};

processTest.prototype.handleOffline = function () {
    //
    // If any timeout is currently pending, cancel it.
    //
    if (this.currentTimeout !== null) {
        clearTimeout(this.currentTimeout);
        this.currentTimeout = null;
    }
    //
    // If any operation is currently underway, cancel it.
    //
    while (this.stack.length) {
        this.stack.pop();
    }
    console.log('offline');
};


/**
 * static function to add Listener to iot_client process
 * @param clientProcesses is array of clientProcess for AWS IOT
 */
processTest.prototype.iot_ClientInitListener = function (clientProcesses){
    clientProcesses[clientProcesses.length-1].thingShadows.on('status', function(thingName, stat, clientToken, stateObject) {
        clientProcesses[clientProcesses.length-1].handleStatus(thingName, stat, clientToken, stateObject);});
    clientProcesses[clientProcesses.length-1].thingShadows.on('delta', function(thingName, stateObject) {
        clientProcesses[clientProcesses.length-1].handleDelta(thingName, stateObject);});
    clientProcesses[clientProcesses.length-1].thingShadows.on('timeout', function(thingName, clientToken) {
        clientProcesses[clientProcesses.length-1].handleTimeout(thingName, clientToken);});
    clientProcesses[clientProcesses.length-1].thingShadows.on('offline', function() {
        clientProcesses[clientProcesses.length-1].handleOffline();});
    return clientProcesses;
};

module.exports = processTest;