let sqLite3 = require('sqlite3').verbose();
let db = new sqLite3.Database('./test.db');

let events = require('events').EventEmitter;
let emitter = new events.EventEmitter();

emitter.on('newEvent', function(user){
    console.log(user);
});

emitter.emit('newEvent', "Krunal");


db.serialize(function() {

    sql_statment = "CREATE TABLE IF NOT EXISTS lorem (info TEXT)";
    // CREATE table IF NOT EXISTS table_name (para1,para2);
    db.run(sql_statment,[],function(err){
        if (err){
            throw "error here"
        }
    });

    let stmt = db.prepare("INSERT INTO lorem VALUES (?)");

    for (let i = 0; i < 10; i++) {
        stmt.run("Ipsum " + i);
    }
    stmt.finalize();


    db.each("SELECT rowid AS id, info FROM lorem", function(err, row) {
        console.log(row.id + ": " + row.info);
    });
    db.close();
});