const session = require('express-session');
const SQLiteStore = require('connect-sqlite3')(session);

function sql_lite_config() {
  if (!(this instanceof sql_lite_config)) {
    return new sql_lite_config();
  }

  this.sessionSetting = {
    store: new SQLiteStore({
      table: 'sessions',
      db: 'sessionsDB',
    }),
    secret: 'Biigggsecret',
    cookie: { maxAge: 7 * 24 * 60 * 60 * 1000 }, // 1 week
    resave: true,
    saveUninitialized: true,
  };
  return this;
}

module.exports = sql_lite_config;
