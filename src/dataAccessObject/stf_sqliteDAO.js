const sqLite3 = require('sqlite3').verbose();
const path = require('path');

const db = new sqLite3.Database(path.join(__dirname, '/../../sft_sqlite.db'));
const pino = require('pino');
const err_handling = require('../stf_generic_js/err_handling');

const logger = pino({ level: process.env.LOG_LEVEL || 'info' });

exports.createIOTTableSchema = function createIOTTableSchema() {
  const sql_statment = 'CREATE TABLE IF NOT EXISTS iot_states (id INTEGER PRIMARY KEY AUTOINCREMENT, thingName TEXT NOT NULL UNIQUE, delta TEXT, get TEXT)';
  // CREATE table IF NOT EXISTS table_name (para1,para2);
  db.run(sql_statment, [], (err) => {
    if (err) {
      logger.error(err);
    }
  });
};

/**
 * static function to add Listener to iot_client process
 * @param thingName is IOT Thing Name
 */
exports.insertThingToIOTTableSchema = function insertThingToIOTTableSchema(thingName) {
  db.serialize(() => {
    const sql_statement = 'INSERT INTO iot_states (thingName, delta, get) VALUES'
            + '(?, ?, ?)';
    const stmt = db.prepare(sql_statement);
    stmt.run(thingName, null, null, (err) => {
      if (err) {
        logger.error(err);
      }
    });
    stmt.finalize();
  });
};

/**
 * static function to add Listener to iot_client process
 * @param callback is function(err, row)
 */
exports.selectAllDataFromIOTTable = function selectAllDataFromIOTTable(callback) {
  db.serialize(() => {
    const sql_statement = 'SELECT * FROM iot_states';
    db.each(sql_statement, callback);
  });
};

/**
 * static function to add Listener to iot_client process
 * @param callback is function(err, row)
 */
exports.selectDataFromIOTTable = function selectDataFromIOTTable(thingName, callback) {
  db.serialize(() => {
    const sql_statement = 'SELECT * FROM iot_states WHERE thingName = ?';
    const data = [thingName];
    const stmt = db.prepare(sql_statement);
    stmt.get(data, callback);
    stmt.finalize();
  });
};

/**
 * static function to add Listener to iot_client process
 * @param thingName
 * @param delta_state
 * @param get_state
 * @param callback is function(err)
 */
exports.updateIOTTable = function updateIOTTable(thingName, delta_state, get_state, callback) {
  db.serialize(() => {
    if (err_handling.isUndefinedOrNull(delta_state)) {
      const sql_update_statement = 'UPDATE iot_states SET get = ? WHERE thingName = ?';
      const data = [get_state, thingName];
      const stmt = db.prepare(sql_update_statement);
      stmt.run(data, callback);
      stmt.finalize();
    } else {
      // let sql_update_statement = `UPDATE iot_states SET delta = ?, WHERE thingName = ?`;
      const sql_update_statement = 'UPDATE iot_states SET delta = ? WHERE thingName = ?';
      const data = [delta_state, thingName];
      const stmt = db.prepare(sql_update_statement);
      stmt.run(data, callback);
      stmt.finalize();
    }
  });
};
