window.myFunction = function myFunction(InstanceId, state_code, aws_service) {
  const form = document.createElement('form');
  form.setAttribute('method', 'post');
  form.setAttribute('action', '/dashboard_action');

  const x = document.createElement('INPUT');
  x.setAttribute('type', 'hidden');
  x.setAttribute('name', 'index_func');
  if (aws_service === 'ec2') {
    if (state_code === '16') {
      x.setAttribute('value', 'stop_ec2_instances');
    } else if (state_code === '80') {
      x.setAttribute('value', 'start_ec2_instances');
    }
  } else if (aws_service === 'rds') {
    form.setAttribute('action', '/rds_action');
    if (state_code === 'available') {
      x.setAttribute('value', 'stop_rds_instances');
    } else if (state_code === 'stopped') {
      x.setAttribute('value', 'start_rds_instances');
    }
  }

  const y = document.createElement('INPUT');
  y.setAttribute('type', 'hidden');
  y.setAttribute('name', 'instanceID');
  y.setAttribute('value', InstanceId);

  form.appendChild(x);
  form.appendChild(y);

  // form.setAttribute('index_func', 'stop_ec2_instances');
  // form.setAttribute('instanceID', InstanceId);
  form.style.display = 'hidden';
  document.body.appendChild(form);
  form.submit();
};
