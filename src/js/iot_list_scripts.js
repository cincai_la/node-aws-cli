window.myIoTFunction = function(thingName, thingArn, aws_service, state) {
    let form = document.createElement('form');
    form.setAttribute('method', 'post');
    form.setAttribute('action', '/iot_list_action');

    let x = document.createElement("INPUT");
    x.setAttribute("type", "hidden");
    x.setAttribute("name", "index_func");
    if (aws_service === 'iot_connect'){
        if (state === 'disconnected'){
            x.setAttribute("value", "connect_iot");
        } else if (state === 'connected') {
            x.setAttribute("value", "disconnect_iot");
        }
    } else if (aws_service === 'iot_update'){
        if (state === 'connected') {
            x.setAttribute("value", "update_iot");
        }
    } else if (aws_service === 'get_iot'){
        if (state === 'connected') {
            x.setAttribute("value", "get_iot");
        }
    } else if (aws_service === 'list_iot_things'){
        x.setAttribute("value", "list_iot_things");
    }
    let y = document.createElement("INPUT");
    y.setAttribute("type", "hidden");
    y.setAttribute("name", "thingName");
    y.setAttribute("value", thingName);

    let inputJSONText = document.getElementById("exampleFormControlTextarea1").value;
    let z = document.createElement("INPUT");
    z.setAttribute("type", "hidden");
    z.setAttribute("name", "inputJSONText");
    z.setAttribute("value", inputJSONText);

    let j = document.createElement("INPUT");
    j.setAttribute("type", "hidden");
    j.setAttribute("name", "iot_status_strings");
    j.setAttribute("value", JSON.stringify(iot_status_strings));

    form.appendChild(x);
    form.appendChild(y);
    form.appendChild(z);
    form.appendChild(j);

    form.style.display = 'hidden';
    document.body.appendChild(form);
    form.submit();
};

window.generate_StateToPublish = function() {
    let rgbValues = {
        red: 0,
        green: 0,
        blue: 0
    };
    rgbValues.red = Math.floor(Math.random() * 255);
    rgbValues.green = Math.floor(Math.random() * 255);
    rgbValues.blue = Math.floor(Math.random() * 255);

    return {
        state: {
            desired: rgbValues
        }
    };
};

window.iot_pageload = function () {
    if (document.getElementById("exampleFormControlTextarea1").value == "") {
        document.getElementById("exampleFormControlTextarea1").value = JSON.stringify(generate_StateToPublish());
    }
};

window.generateRandomState = function () {
    document.getElementById("exampleFormControlTextarea1").value = JSON.stringify(generate_StateToPublish());
};

window.checkJSONInput = function (elementID){
    let check_text = document.getElementById(elementID).value;
    try {
        JSON.parse(check_text);
    } catch (e) {
        alert('INPUT text is not JSON!');
    }
};

window.getIOTDBState = function (thingName){
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/iot_list_action', true);
    let params = 'index_func=get_json_iot&thingName='+thingName;
    //Send the proper header information along with the request
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.responseType = 'json';
    xhr.onreadystatechange = function() {
        // if (xhr.readyState == 3) {
        //     // loading
        // }
        if (xhr.readyState == 4) {
            let output_text = "";
            let responseObj = xhr.response;
            iot_status_strings = responseObj;
            output_text = JSON.stringify(iot_status_strings, undefined,2);
            document.getElementById("exampleFormControlTextarea2").value = output_text;
        }
    };
    xhr.send(params);
}