#!/usr/bin/env node

function execute(command,callback) {
    const exec = require('child_process').exec

    exec(command, (err, stdout, stderr) => {
        // process.stdout.write(stdout)
        callback(err,stdout);
    })
}

module.exports = execute;
