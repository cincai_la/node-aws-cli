#!/usr/bin/env node

function execute(command,callback) {
    const exec = require('child_process').exec

    exec(command, (err, stdout, stderr) => {
        // process.stdout.write(stdout)
        callback(err,stdout);
    })
}

execute('aws ec2 describe-instances',function(err, data){
    console.log(data);
    console.log(err);
});
