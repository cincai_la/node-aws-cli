let execute = require('./execute');

class aws_ec2_calls  {

    get_status(callback){
        // execute('aws ec2 describe-instances --instance-ids i-0553794b1179d8126 i-0052cad78fac8dbe3',callback);
        execute('aws ec2 describe-instances',callback);
    }

    stop_server(instanceId, callback){
        let command_aws_ec2 = 'aws ec2 stop-instances --instance-id ' +instanceId;
        execute(command_aws_ec2,callback);
    }

    start_server(instanceId, callback){
        let command_aws_ec2 = 'aws ec2 start-instances --instance-ids ' +instanceId;
        execute(command_aws_ec2,callback);
    }
}

class aws_rds_calls  {

    get_status(callback){
        execute('aws rds describe-db-instances',callback);
    }

    stop_server(instanceId, callback){
        let command_aws_ec2 = 'aws rds stop-db-instance --db-instance-identifier ' +instanceId;
        execute(command_aws_ec2,callback);
    }

    start_server(instanceId, callback){
        let command_aws_ec2 = 'aws rds start-db-instance --db-instance-identifier ' +instanceId;
        execute(command_aws_ec2,callback);
    }
}

class aws_iot_calls  {

    get_status(callback){
        execute('aws iot list-things',callback);
    }
}

class aws_s3_calls  {

    s3_bucket_list(callback){
        execute('aws s3 ls',callback);
    }
}

module.exports = {
    AwsEc2Calls : aws_ec2_calls,
    AwsRdsCalls : aws_rds_calls,
    aws_iot_calls: aws_iot_calls,
    AwsS3Calls: aws_s3_calls
}
