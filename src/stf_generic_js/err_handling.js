let blah = require('./err_handling');
/**
 * To determine undefined and null.
 *
 * @param {Object} value
 * @access public
 */
exports.isUndefinedOrNull = function(value) {
    return typeof value === 'undefined' || value === null ||  value === 'null';
};

exports.isUndefinedOrNull_ReturnValue = function(value) {
    if (typeof value === 'undefined' || value === null ||  value === 'null'){
        return null;
    } else {
        return JSON.parse(value);
    }
};

exports.checkJSONStringify = function(value){
    if (blah.isUndefinedOrNull(value)){
        return null
    } else {
        return JSON.stringify(value);
    }
};