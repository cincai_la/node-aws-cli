const path = require('path');
const splitBW = require('string-split-by-whitespace');
const awsCliCalls = require('../server_only_js/aws_cli_calls');
const indexTableCalls = require('../views/js_model/index_table_obj');
const indexObjects = require('../views/js_model/indexObjs');

function listS3Buckets(req, res) {
  const indexObjs = new indexObjects.IndexObjs(req);
  const s3Caller = new awsCliCalls.AwsS3Calls();
  s3Caller.s3_bucket_list((err, data) => {
    // let obj = JSON.parse(data);
    const obj = splitBW.splitByW(data);
    const s3BucketList = [];
    for (let j = 0; j < obj.length / 3; j += 1) {
      const s3CreatedDate = obj[0 + j * 3];
      const s3CreatedTime = obj[1 + j * 3];
      const s3BucketName = obj[2 + j * 3];
      const s3BucketObj = new indexTableCalls.S3TableObj(s3CreatedDate,
        s3CreatedTime, s3BucketName);
      s3BucketList.push(s3BucketObj);
    }
    indexObjs.s3_table_obj_list = s3BucketList;
    indexObjs.setReqSession(req);
    res.render(path.join(__dirname, '/../views/s3.html'), { s3_buckets: indexObjs.s3_table_obj_list });
  });
}

function s3PageCommand(req, res) {
  if (req.body.index_func === indexTableCalls.general_mapping.index_func.get_s3_buckets) {
    listS3Buckets(req, res);
  }
}

module.exports = {
  s3PageCommand,
};
