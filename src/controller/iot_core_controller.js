const path = require('path');
const awsCliCalls = require('../server_only_js/aws_cli_calls');
const indexTableCalls = require('../views/js_model/index_table_obj');
const indexObjects = require('../views/js_model/indexObjs');
const err_handling = require('../stf_generic_js/err_handling');
const ProcessTest = require('../views/js_model/iot_update_obj');

let clientProcessTests = [];
const stf_sqliteDAO = require('../dataAccessObject/stf_sqliteDAO');

function list_iot_devices(req, res) {
  const index_objs = new indexObjects.IndexObjs(req);
  const iot_caller = new awsCliCalls.aws_iot_calls();
  index_objs.iot_status_strings = err_handling.isUndefinedOrNull_ReturnValue(req.body.iot_status_strings);
  index_objs.inputIOTJSONText = JSON.parse(req.body.inputJSONText);
  iot_caller.get_status((err, data) => {
    const obj = JSON.parse(data);
    const iot_things = obj.things;
    const iot_table_temp = [];
    for (let i = 0; i < iot_things.length; i++) {
      const temp_data = new indexTableCalls.iot_core_table_obj(iot_things[i].thingName, iot_things[i].thingArn, 'disconnected');
      iot_table_temp.push(temp_data);
      stf_sqliteDAO.insertThingToIOTTableSchema(iot_things[i].thingName);
    }
    index_objs.iot_table_table_obj_list = iot_table_temp;
    stf_sqliteDAO.selectAllDataFromIOTTable((err, row) => {
      console.log(`${row.id}: ${row.thingName}`);
    });
    index_objs.setReqSession(req);
    res.render(path.join(__dirname, '/../views/iot_list.html'), {
      iot_table: index_objs.iot_table_table_obj_list,
      inputJSONText: JSON.stringify(index_objs.inputIOTJSONText),
      iot_status_strings: JSON.stringify(req.session.iot_status_strings),
    });
  });
}

// register the things, (subscribed and published)
function connect_iot(req, res) {
  const index_objs = new indexObjects.IndexObjs(req);
  const thing_Name = req.body.thingName;
  index_objs.iot_status_strings = err_handling.isUndefinedOrNull_ReturnValue(req.body.iot_status_strings);
  index_objs.inputIOTJSONText = JSON.parse(req.body.inputJSONText);
  const process_Test = new ProcessTest(thing_Name, stf_sqliteDAO);
  let has_thing = false;
  process_Test.myDeviceConnect((err, failedTopic) => {
    if (err_handling.isUndefinedOrNull(err) && err_handling.isUndefinedOrNull(failedTopic)) {
      for (let i = 0; i < index_objs.iot_table_table_obj_list.length; i++) {
        if (index_objs.iot_table_table_obj_list[i].thingName === thing_Name) {
          index_objs.iot_table_table_obj_list[i].state = indexTableCalls.general_mapping.iot_state.connected;
          for (let i = 0; i < clientProcessTests.length; i++) {
            if (clientProcessTests[i].thingName === thing_Name) {
              has_thing = true;
            }
          }
        }
      }
      if (!has_thing) {
        clientProcessTests.push(process_Test);
        clientProcessTests = process_Test.iot_ClientInitListener(clientProcessTests);
      }
      index_objs.setReqSession(req);
      res.render(path.join(__dirname, '/../views/iot_list.html'), {
        iot_table: index_objs.iot_table_table_obj_list,
        inputJSONText: JSON.stringify(index_objs.inputIOTJSONText),
        iot_status_strings: JSON.stringify(req.session.iot_status_strings),
      });
    } else {
      index_objs.setReqSession(req);
      res.render(path.join(__dirname, '/../views/iot_list.html'), {
        iot_table: index_objs.iot_table_table_obj_list,
        inputJSONText: JSON.stringify(index_objs.inputIOTJSONText),
        iot_status_strings: JSON.stringify(req.session.iot_status_strings),
      });
    }
  });
}

function disconnect_iot(req, res) {
  const index_objs = new indexObjects.IndexObjs(req);
  const thing_Name = req.body.thingName;
  index_objs.iot_status_strings = err_handling.isUndefinedOrNull_ReturnValue(req.body.iot_status_strings);
  index_objs.inputIOTJSONText = JSON.parse(req.body.inputJSONText);
  for (let i = 0; i < index_objs.iot_table_table_obj_list.length; i++) {
    if (index_objs.iot_table_table_obj_list[i].thingName === thing_Name) {
      index_objs.iot_table_table_obj_list[i].state = indexTableCalls.general_mapping.iot_state.disconnected;
      for (let i = 0; i < clientProcessTests.length; i++) {
        if (clientProcessTests[i].thingName === thing_Name) {
          clientProcessTests[i].myDeviceUnregister();
          clientProcessTests.splice(0, 1);
        }
      }
    }
  }
  index_objs.setReqSession(req);
  res.render(path.join(__dirname, '/../views/iot_list.html'), {
    iot_table: index_objs.iot_table_table_obj_list,
    inputJSONText: JSON.stringify(index_objs.inputIOTJSONText),
    iot_status_strings: JSON.stringify(req.session.iot_status_strings),
  });
}

function update_iot(req, res) {
  const index_objs = new indexObjects.IndexObjs(req);
  const thing_Name = req.body.thingName;
  index_objs.iot_status_strings = err_handling.isUndefinedOrNull_ReturnValue(req.body.iot_status_strings);
  const input_json_obj = JSON.parse(req.body.inputJSONText);
  index_objs.inputIOTJSONText = JSON.parse(req.body.inputJSONText);
  for (let i = 0; i < index_objs.iot_table_table_obj_list.length; i++) {
    if (index_objs.iot_table_table_obj_list[i].thingName === thing_Name) {
      for (let i = 0; i < clientProcessTests.length; i++) {
        if (clientProcessTests[i].thingName === thing_Name) {
          clientProcessTests[i].updateThing(input_json_obj, (err, clientToken) => {
            if (!err) {
              clientProcessTests[i].stack.push(clientToken);
            }
          });
        }
      }
    }
  }
  index_objs.setReqSession(req);
  res.render(path.join(__dirname, '/../views/iot_list.html'), {
    iot_table: index_objs.iot_table_table_obj_list,
    inputJSONText: JSON.stringify(index_objs.inputIOTJSONText),
    iot_status_strings: JSON.stringify(req.session.iot_status_strings),
  });
}

function get_iot(req, res) {
  const index_objs = new indexObjects.IndexObjs(req);
  const thing_Name = req.body.thingName;
  index_objs.iot_status_strings = err_handling.isUndefinedOrNull_ReturnValue(req.body.iot_status_strings);
  index_objs.inputIOTJSONText = JSON.parse(req.body.inputJSONText);
  for (let i = 0; i < index_objs.iot_table_table_obj_list.length; i++) {
    if (index_objs.iot_table_table_obj_list[i].thingName === thing_Name) {
      for (let i = 0; i < clientProcessTests.length; i++) {
        if (clientProcessTests[i].thingName === thing_Name) {
          clientProcessTests[i].get_IOT_status((err, clientToken) => {
            if (!err) {
              clientProcessTests[i].stack.push(clientToken);
            }
          });
        }
      }
    }
  }
  index_objs.setReqSession(req);
  res.render(path.join(__dirname, '/../views/iot_list.html'), {
    iot_table: index_objs.iot_table_table_obj_list,
    inputJSONText: JSON.stringify(index_objs.inputIOTJSONText),
    iot_status_strings: JSON.stringify(req.session.iot_status_strings),
  });
}

function get_json_iot(req, res) {
  const index_objs = new indexObjects.IndexObjs(req);
  const thing_Name = req.body.thingName;
  for (let i = 0; i < index_objs.iot_table_table_obj_list.length; i++) {
    if (index_objs.iot_table_table_obj_list[i].thingName === thing_Name) {
      stf_sqliteDAO.selectDataFromIOTTable(thing_Name, (err, row) => {
        if (!err) {
          index_objs.setReqSession(req);
          const output_text = { thingName: row.thingName, delta: JSON.parse(row.delta), get: JSON.parse(row.get) };
          console.log(JSON.stringify(output_text, undefined, 2));

          res.json(output_text);
        } else {
          res.json({ message: 'timeout' });
        }
      });
    }
  }
}

function iot_core_PageCommand(req, res) {
  if (req.body.index_func === indexTableCalls.general_mapping.index_func.list_iot_things) {
    list_iot_devices(req, res);
  } else if (req.body.index_func === indexTableCalls.general_mapping.index_func.connect_iot) {
    connect_iot(req, res);
  } else if (req.body.index_func === indexTableCalls.general_mapping.index_func.disconnect_iot) {
    disconnect_iot(req, res);
  } else if (req.body.index_func === indexTableCalls.general_mapping.index_func.update_iot) {
    update_iot(req, res);
  } else if (req.body.index_func === indexTableCalls.general_mapping.index_func.get_iot) {
    get_iot(req, res);
  } else if (req.body.index_func === indexTableCalls.general_mapping.index_func.get_json_iot) {
    get_json_iot(req, res);
  }
}

module.exports = {
  iot_core_PageCommand,
};
