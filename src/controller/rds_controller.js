// Load required packages
const path = require('path');
const awsCliCalls = require('../server_only_js/aws_cli_calls');
const indexTableCalls = require('../views/js_model/index_table_obj');
const indexObject = require('../views/js_model/indexObjs');

// Create endpoint for RDS describeInstances
function describeRDSInstances(req, res) {
  const rdsCaller = new awsCliCalls.AwsRdsCalls();
  const indexObjs = new indexObject.IndexObjs(req);
  rdsCaller.get_status((err, data) => {
    const obj = JSON.parse(data);
    const indexRdsTable = [];
    const { DBInstances } = obj;
    for (let i = 0; i < DBInstances.length; i += 1) {
      const tempData = new indexTableCalls.IndexRdsTableObj(DBInstances[i].DBName,
        DBInstances[i].DBInstanceIdentifier, DBInstances[i].DBInstanceClass,
        DBInstances[i].DBInstanceStatus);
      indexRdsTable.push(tempData);
    }
    indexObjs.index_rds_table_obj_list = indexRdsTable;
    indexObjs.setReqSession(req);
    res.render(path.join(__dirname, '/../views/rds_list.html'), { index_table: indexObjs.index_ec2_table_obj_list, index_rds_table: indexObjs.index_rds_table_obj_list });
  });
}

// Create endpoint for RDS stopInstances
function stopRDSInstance(req, res) {
  const indexObjs = new indexObject.IndexObjs(req);
  if (req.session.index_rds_table) {
    const thisInstanceID = req.body.instanceID;
    const rdsCaller = new awsCliCalls.AwsRdsCalls();
    rdsCaller.stop_server(thisInstanceID, (err, data) => {
      const obj = JSON.parse(data);
      const instanceID = obj.DBInstance.DBInstanceIdentifier;
      const dbClass = obj.DBInstance.DBInstanceClass;
      const status = obj.DBInstance.DBInstanceStatus;
      const { DBName } = obj.DBInstance;
      // let index_rds_table = req.session.index_rds_table;
      for (let i = 0; i < indexObjs.index_rds_table_obj_list.length; i += 1) {
        if (indexObjs.index_rds_table_obj_list[i].instanceId === instanceID) {
          indexObjs.index_rds_table_obj_list[i].name = DBName;
          indexObjs.index_rds_table_obj_list[i].instance_type = dbClass;
          indexObjs.index_rds_table_obj_list[i].status = status;
        }
      }
      indexObjs.setReqSession(req);
      res.render(path.join(__dirname, '/../views/rds_list.html'), { index_table: indexObjs.index_ec2_table_obj_list, index_rds_table: indexObjs.index_rds_table_obj_list });
    });
  }
}

// Create endpoint for RDS stopInstances
function startRDSInstance(req, res) {
  const indexObjs = new indexObject.IndexObjs(req);
  if (req.session.index_rds_table) {
    const thisInstanceID = req.body.instanceID;
    const rdsCaller = new awsCliCalls.AwsRdsCalls();
    rdsCaller.start_server(thisInstanceID, (err, data) => {
      const obj = JSON.parse(data);
      const instanceID = obj.DBInstance.DBInstanceIdentifier;
      const dbClass = obj.DBInstance.DBInstanceClass;
      const status = obj.DBInstance.DBInstanceStatus;
      const { DBName } = obj.DBInstance;
      for (let i = 0; i < indexObjs.index_rds_table_obj_list.length; i += 1) {
        if ([i].instanceId === instanceID) {
          indexObjs.index_rds_table_obj_list[i].name = DBName;
          indexObjs.index_rds_table_obj_list[i].instance_type = dbClass;
          indexObjs.index_rds_table_obj_list[i].status = status;
        }
      }
      indexObjs.setReqSession(req);
      res.render(path.join(__dirname, '/../views/rds_list.html'), { index_table: indexObjs.index_ec2_table_obj_list, index_rds_table: indexObjs.index_rds_table_obj_list });
    });
  }
}

function rdsPageCommand(req, res) {
  // factory
  if (req.body.index_func === indexTableCalls.general_mapping.index_func.describe_rds_instances) {
    describeRDSInstances(req, res);
  } else if (req.body.index_func === indexTableCalls.general_mapping
    .index_func.stop_rds_instances) {
    stopRDSInstance(req, res);
  } else if (req.body.index_func === indexTableCalls.general_mapping
    .index_func.start_rds_instances) {
    startRDSInstance(req, res);
  }
}

module.exports = {
  rdsPageCommand,
};
