// Load required packages
const path = require('path');
const awsCliCalls = require('../server_only_js/aws_cli_calls');
const indexTableCalls = require('../views/js_model/index_table_obj');
const rdsController = require('./rds_controller');
const IndObjs = require('../views/js_model/indexObjs');

// Create endpoint for ec2 describeInstances
// describeEC2Instances = function (req, res) {
function describeAWSEC2Instances(req, res) {
  const indexObjs = new IndObjs.IndexObjs(req);
  const ec2Caller = new awsCliCalls.AwsEc2Calls();
  ec2Caller.get_status((err, data) => {
    const obj = JSON.parse(data);
    const indexEc2Table = [];
    const ec2Reservations = obj.Reservations;
    for (let i = 0; i < ec2Reservations.length; i += 1) {
      const ec2Instances = ec2Reservations[i].Instances;
      const tempData = new indexTableCalls.IndexEc2TableObj(ec2Instances[0].Tags[0].Value,
        ec2Instances[0].State.Code, ec2Instances[0].State.Name, ec2Instances[0].InstanceType,
        ec2Instances[0].InstanceId, ec2Instances[0].PublicIpAddress);
      indexEc2Table.push(tempData);
    }
    indexObjs.index_ec2_table_obj_list = indexEc2Table;
    indexObjs.setReqSession(req);
    res.render(path.join(__dirname, '/../views/index.html'), { index_table: indexObjs.index_ec2_table_obj_list, index_rds_table: indexObjs.index_rds_table_obj_list });
  });
}

// Create endpoint for ec2 stopInstances
// stopEC2Instance = function stopAWSEC2Instance(req, res) {
function stopAWSEC2Instance(req, res) {
  const indexObjs = new IndObjs.IndexObjs(req);
  if (req.session.index_table) {
    const thistInstanceID = req.body.instanceID;
    const ec2Caller = new awsCliCalls.AwsEc2Calls();
    ec2Caller.stop_server(thistInstanceID, (err, data) => {
      const obj = JSON.parse(data);
      const instanceID = obj.StoppingInstances[0].InstanceId;
      const currentState = obj.StoppingInstances[0].CurrentState;
      const previousState = obj.StoppingInstances[0].PreviousState;
      // let index_table = req.session.index_table;
      const tempData = new indexTableCalls.IndexStoppingObj(instanceID,
        currentState, previousState);
      for (let i = 0; i < indexObjs.index_ec2_table_obj_list.length; i += 1) {
        if (indexObjs.index_ec2_table_obj_list[i].InstanceId === instanceID) {
          indexObjs.index_ec2_table_obj_list[i].state_code = tempData.CurrentState.Code;
          indexObjs.index_ec2_table_obj_list[i].state_name = tempData.CurrentState.Name;
        }
      }
      indexObjs.setReqSession(req);
      res.render(path.join(__dirname, '/../views/index.html'), { index_table: indexObjs.index_ec2_table_obj_list, index_rds_table: indexObjs.index_rds_table_obj_list });
    });
  }
}

// Create endpoint for ec2 stopInstances
// startEC2Instance = function (req, res) {
function startAWSEC2Instance(req, res) {
  const indexObjs = new IndObjs.IndexObjs(req);
  if (req.session.index_table) {
    const thistInstanceID = req.body.instanceID;
    const ec2Caller = new awsCliCalls.AwsEc2Calls();
    ec2Caller.start_server(thistInstanceID, (err, data) => {
      const obj = JSON.parse(data);
      const instanceID = obj.StartingInstances[0].InstanceId;
      const currentState = obj.StartingInstances[0].CurrentState;
      const previousState = obj.StartingInstances[0].PreviousState;
      const tempData = new indexTableCalls.IndexStoppingObj(instanceID,
        currentState, previousState);

      for (let i = 0; i < indexObjs.index_ec2_table_obj_list.length; i += 1) {
        if (indexObjs.index_ec2_table_obj_list[i].InstanceId === instanceID) {
          indexObjs.index_ec2_table_obj_list[i].state_code = tempData.CurrentState.Code;
          indexObjs.index_ec2_table_obj_list[i].state_name = tempData.CurrentState.Name;
        }
      }
      indexObjs.setReqSession(req);
      res.render(path.join(__dirname, '/../views/index.html'), { index_table: indexObjs.index_ec2_table_obj_list, index_rds_table: indexObjs.index_rds_table_obj_list });
    });
  }
}

function indexPageCommand(req, res) {
  if (req.body.index_func === indexTableCalls.general_mapping.index_func.describe_ec2_instances) {
    // this.describeEC2Instances(req, res);
    describeAWSEC2Instances(req, res);
    // describeEC2Instances
  } else if (req.body.index_func === indexTableCalls.general_mapping
    .index_func.stop_ec2_instances) {
    stopAWSEC2Instance(req, res);
  } else if (req.body.index_func === indexTableCalls.general_mapping
    .index_func.start_ec2_instances) {
    startAWSEC2Instance(req, res);
  } else if (req.body.index_func === indexTableCalls.general_mapping
    .index_func.describe_rds_instances) {
    rdsController.indexPageCommand(req, res);
  } else if (req.body.index_func === indexTableCalls.general_mapping
    .index_func.stop_rds_instances) {
    rdsController.indexPageCommand(req, res);
  } else if (req.body.index_func === indexTableCalls.general_mapping
    .index_func.start_rds_instances) {
    rdsController.indexPageCommand(req, res);
  }
}

module.exports = {
  indexPageCommand,
};
