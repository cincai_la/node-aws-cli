#From node:15.12
From nikolaik/python-nodejs:python3.7-nodejs15
WORKDIR /app
COPY package*.json ./
COPY . .
RUN npm install
RUN pip install awscli
RUN npm run build:db_only
RUN npm run build
CMD ["node", "src/server.js"]
