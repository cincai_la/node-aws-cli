# Node JS, Bootstrap, EJS, AWS CLI (Demo Project)

## About
This is quickly designed just for personal use case to list EC2 instances, RDS instances, S3 buckets, and IOT devices. AWS cli command is obtained from designed button with javascript function. The javascript is minified using webpack.

## Requirements
see package.json

### Docker build, and then Docker run
the docker build file is provided
```
docker build -t <image name> .
```

as it is designed to run locally, aws config detail is to be shared with the container with the built image.
```
docker run -p 127.0.0.1:5001:5001 --name <container name> -v ~/.aws:/root/.aws <image name>
```

## EC2 Page and RDS Page

#### EC2 list or RDS list
The EC2 page is designed to be able to get the public IPs of EC2 instances. Similarly, RDS page is to list account RDS instance.

#### EC2 start
By pressing the start button, EC2 instance will be started (NOTE: not creating.). Similary, RDS instance will be started.

#### EC2 stop
By pressing the stop button, EC2 instance will be stopped (NOTE: not terminated.). Similarly, RDS instance can be stopped (not much cost benefit).

### EC2 page
![EC2 Page](https://drive.google.com/uc?id=1woG4wzrLktfAJ0YcG3KCNDospUHBbjwP)

### RDS page
![RDS Page](https://drive.google.com/uc?id=1SgRB3_DeQlZBAs01moYcRDvyaqfA-_yf)

### IOT page
![IOT Page](https://drive.google.com/uc?id=1e9nUDXG-ldzDFDLfo_VgXm9FAureqiMO)

### S3 page
![S3 Page](https://drive.google.com/uc?id=1ekSYn1KDjQ8DXYWBmksEQddBYT_Gi-WG)


## IOT Certs and keys
* this is not for everyone. let me know if you need help.

## Bugs
* a lot.
